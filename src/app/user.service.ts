import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  constructor(private httpService: HttpService) { }

  getUsers(): Observable<any> {
    return this.httpService.getUsers();
  }

  getUserById(id: number): Observable<any> {
    return this.httpService.getUserById(id);
  }

  addUser(user: any): Observable<any> {
    return this.httpService.addUser(user);
  }

  updateUser(id: number, user: any): Observable<any> {
    return this.httpService.updateUser(id, user);
  }

  deleteUser(id: number): Observable<any> {
    return this.httpService.deleteUser(id);
  }
}

