import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../user.service';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss']
})
export class AddUserComponent {
  newUser = { name: '', email: '', occupation: '', bio: '' };

  constructor(private userService: UserService, private router: Router) {}

  addUser(): void {
    this.userService.addUser(this.newUser).subscribe(() => {
      this.router.navigate(['/users']);
    });
  }
}

