import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../user.service';

@Component({
  selector: 'app-update-user',
  templateUrl: './update-user.component.html',
  styleUrls: ['./update-user.component.scss']
})
export class UpdateUserComponent implements OnInit {
  user: any = {};

  constructor(
    private userService: UserService, 
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    const userId = this.route.snapshot.paramMap.get('id');
    if (userId) {
      this.userService.getUserById(+userId).subscribe(data => {
        this.user = data;
      });
    }
  }

  updateUser(): void {
    this.userService.updateUser(this.user.id, this.user).subscribe(() => {
      this.router.navigate(['/users']);
    });
  }
}

